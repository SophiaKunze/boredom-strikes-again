import axios from "axios"

const API_URL = "https://www.boredapi.com/api/activity" 

export const getActivity = async () => {
    console.log("getActivity()");
    try {
        const response = await axios.get(API_URL);
        const data = await response.data
        console.log(response)
        console.log(data)
        return [null, data]
        }
     catch (error) {
        console.log(error);
        return [error.message, null]
    }
} 