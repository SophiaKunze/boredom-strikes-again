import './App.css';
import Activity from './components/Activity';

function App() {
    return (
        <div className="App">
            <main className="container">
                <Activity/>
            </main>
        </div>
    );
}

export default App;
