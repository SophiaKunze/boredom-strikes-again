import {useEffect, useState} from "react";
import {getActivity} from "../api/activity";

const Activity = () => {
    const [loading, setLoading] = useState(false)
    const [activity, setActivity] = useState(null)

    const onActivityClick = async () => {
        console.log("Button clicked");
        setActivity(null)
        setLoading(true)
        const newActivity = await getActivity() // returns an array [error, data]
        setActivity(newActivity[1])
        console.log("activity set.: ", newActivity[1].activity) // TODO: works
        setLoading(false)
        console.log("activity.activity: ", activity.activity); // TODO why error when initial state of activity is null or {}?
    }

    return (
        <>
            <container>
                <h1>Are you bored? Choose a new activity!</h1>
                <button type="button" onClick={onActivityClick}>New Activity</button>
                {activity && <p>How about the {activity.type} activity <span
                    id="activity">'{activity.activity}'</span> for {activity.participants} participants for
                    price-level {activity.price}? </p>}
                {loading && <p>Loading...</p>}
            </container>
        </>
    );
};

export default Activity;
