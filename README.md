# Boredom Strikes Again

This React app uses the "Bored [API](https://www.boredapi.com/)" to suggest activities against boredom.

This repository contains:
1. [package-lock.json](package-lock.json) listing versions of installed packages
2. [package.json](package.json) listing metadata, dependencies and scripts
3. [src](src) containing the source code
4. [public](public) containing static files such as index.html and images

## Table of Contents
- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background
This mini app is a group work within the scope of a full stack development course by <a href="https://www.noroff.no/en/">Noroff</a>.

## Install
Clone repository via `git clone`. 

## Usage
The user can click a button and receives activities against boredom.

## Maintainers
[@hubermarkus](https://gitlab.com/hubermarkus) and [@SophiaKunze](https://gitlab.com/SophiaKunze)

## Contributing
This projects follows the [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) Code of Conduct.

## License
[MIT](https://opensource.org/licenses/MIT) © Markus Huber and Sophia Kunze
